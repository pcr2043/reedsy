import Vue from 'vue';
import Axios from "axios";

// Object for books endpoit
let booksData = {
  books: [],
  count: -1
}


beforeAll((done) => {

  // get data form api
  Axios.get('http://localhost:3000/books').then(response => {
    let books = response.data.books;
    let count = response.data.meta.count;

    booksData.books = books;
    booksData.count = count;
    done();
  })
});


it('API - It should match books length', function (done) {

  expect(booksData.books.length).toBe(booksData.count);
  done();

});