require('./boostrap');

import Vue from 'vue';
import VueRouter from 'vue-router'

Vue.use(VueRouter)

// Share a vue instance for Events
window.Bus = new Vue;

// Import Books Service
import { BookService, HelperService } from './services/global';

// Register Main View Components
import Home from './components/views/home.vue';
import justAdded from './components/views/justAdded.vue';
import topBooksAllTime from './components/views/topBooksAllTime.vue';
import bookDetails from './components/views/bookDetails.vue';
// 2. Define some routes
// Each route should map to a component. The "component" can
// either be an actual component constructor created via
// `Vue.extend()`, or just a component options object.
// We'll talk about nested routes later.
const routes = [
  { path: '/', component: Home },
  { path: '/just-added', component: justAdded },
  { path: '/top-books', component: Home },
  { path: '/top-books-all-time', component: topBooksAllTime },
  { path: '/top-upcomming', component: Home },
  { path: '/book/:id', component: bookDetails }
]

// 3. Create the router instance and pass the `routes` option
// You can pass in additional options here, but let's
// keep it simple for now.
const router = new VueRouter({
  routes // short for `routes: routes`
})


const app = new Vue({
  router,
  el: "#app",
  data() {
    return {
      loaded: false,
      bookserviceLoaded: false
    }
  },
  mounted() {
    console.log("app mounted")

    // Register Book Service, we are using with in all pages// so can be shared
    window.bookService = new BookService();
    window.helperService = new HelperService();

    Bus.$on('resource-loaded', resource => {

      console.log("resource loaded: " + resource)
      switch (resource) {
        case 'book-service': {
          this.bookServiceLoaded = true
        }
      }

      // Every time resource is loade check all resources
      // in this case i just one resource bookService, cause heperService has no lazy data
      // this is necessary to avoid components of been rendered without services ready
      // is perfomance technique :)
      if (this.bookServiceLoaded)
        this.loaded = true;
    })
  }

});
