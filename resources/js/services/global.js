

export class BookService {
  constructor() {
    this.books = [];
    this.bind();
  }

  bind() {
    axios.get('/books').then(response => {
      this.books = response.data.books;

      // Emit resource loaded
      Bus.$emit('resource-loaded', 'book-service')
    })
  }

  getTopRating() {
    return this.books.sort(function (a, b) { return b.rating - a.rating })
  }

  getTopVoted() {
    return this.books.sort(function (a, b) { return b.upvotes - a.upvotes })
  }
}

export class HelperService {
  random(min, max) {
    return Math.floor(Math.random() * (+max - +min)) + +min;
  }
}