'use strict';

import Hapi from 'hapi';
import Inert from 'inert';



import routes from './routes';

const Path = require('path');


const server = new Hapi.Server({
  port: 3000,

  routes: {
    cors: true,

    files: {
      relativeTo: Path.join(__dirname, '../public')
    }
  }

});

const startServer = async () => {
  await server.register(Inert);


  server.route({
    method: 'GET',
    path: '/{param*}',
    handler: {
      directory: {
        path: '.',
        redirectToSlash: true
      }
    }
  });

  routes.forEach((route) => {
    server.route(route);
  });

  try {
    await server.start();

    // Quick fix for multiple for environment cause we are using dev and prod and in prod port 3000 no accescible
    let uri = process.argv[3].replace('URI=', '');
    console.info(`********************* SERVER INFO ***************************`);
    console.info(`Server started at ${uri}`);
    console.info(`*************************************************************`);
  } catch (err) {
    console.error(err);
  }
}

process.on('unhandledRejection', (err) => {
  console.error(err);
  process.exit(1);
});

startServer();
